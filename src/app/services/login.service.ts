import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, Observable, of, switchMap, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class LoginService {

    httpOptions: any;
    private _authenticated: boolean = false;
    accessToken: any = sessionStorage.getItem('accessToken')  ?? '';

    constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient,) {
      // this.token = sessionStorage.getItem('token');
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };
    }

    //ฟังก์ชั่น Check Login
    login(info: any) {
      //รับค่า route login
      const url: any = `${this.apiUrl}/login/fuse`;
      // console.log(url);
      //ส่งค่าไปยัง route login
      return this.httpClient.post(url,info,this.httpOptions).toPromise();
    }

   /**
     * Check the authentication status
     */
   check(): Observable<boolean>
   {
      this.accessToken = sessionStorage.getItem('accessToken')  ?? '';
       // Check if the user is logged in
       if ( this.accessToken && this.accessToken != '' )
       {
          this._authenticated = true;
           return of(true);
       }

       // Check the access token availability
       if ( this.accessToken == '' || !this.accessToken)
       {
          this._authenticated = false;
           return of(false);
       }
   }

   signOut(): Observable<any>
   {
       // Remove the access token from the local storage
       sessionStorage.removeItem('accessToken');

       // Set the authenticated flag to false
       this._authenticated = false;

       // Return the observable
       return of(true);
   }

  }
