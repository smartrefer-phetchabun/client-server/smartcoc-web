import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MotorPowerService {

  //   tokenn: any = "";
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    // this.token = sessionStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcm92Y29kZSI6IjM0IiwicHJvdm5hbWUiOiLguK3guLjguJrguKXguKPguLLguIrguJjguLLguJnguLUiLCJmdWxsbmFtZSI6IuC4mOC4p-C4seC4iuC4iuC4seC4oiDguYHguKrguIfguYDguJTguLfguK3guJkiLCJpYXQiOjE2NzMzMjg0NTcsImV4cCI6MTcwNDg4NjA1N30.EC5XvMBjmKrTKGT2wfu9vnt0j5SNin5O3xpSCuKhbGA'
      })
    };
  }

  async list() {
    const _url = `${this.apiUrl}/question/question`;
    let info = {
      "question_type_id": 14
    }
    return await this.httpClient.post(_url, info).toPromise();
  }
  async saveEvaluate(info: object) {
    const _url = `${this.apiUrl}/coc_evaluate/insert`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }
}