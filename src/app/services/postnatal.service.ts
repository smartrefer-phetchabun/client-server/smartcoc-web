import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostnatalService {

  accessToken :any;
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    this.accessToken = sessionStorage.getItem('accessToken');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.accessToken
      })
    };
  }

  // async list() {
  //   console.log("list");

  //   const _url = `${this.apiUrl}/question/question`;
  //   let info = {
  //     "question_type_id": 15
  //   }
  //   return this.httpClient.get(_url, this.httpOptions).toPromise();

  // }

  async list(){
    const _url = `${this.apiUrl}/question/question`;
    let info= {
      "question_type_id" : 15
    }
   return await this.httpClient.post(_url,info).toPromise();
}
  async saveEvaluate(info: object) {
    const _url = `${this.apiUrl}/coc_evaluate/insert`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

}