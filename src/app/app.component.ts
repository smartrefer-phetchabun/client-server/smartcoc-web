import { Component } from '@angular/core';
import { AuthUtils } from 'app/core/auth/auth.utils';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector   : 'app-root',
    templateUrl: './app.component.html',
    styleUrls  : ['./app.component.scss']
})
export class AppComponent
{
    public dataLogin:any;
    /**
     * Constructor
     */
    constructor(@Inject('REFER_URL') private referUrl: string,private router:Router )
    {

        // localStorage.removeItem('accessToken');
        // eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcm92Y29kZSI6IjM0IiwicHJvdm5hbWUiOiLguK3guLjguJrguKXguKPguLLguIrguJjguLLguJnguLUiLCJmdWxsbmFtZSI6IuC4mOC4p-C4seC4iuC4iuC4seC4oiDguYHguKrguIfguYDguJTguLfguK3guJkiLCJpYXQiOjE2NzMzMjg0NTcsImV4cCI6MTcwNDg4NjA1N30.EC5XvMBjmKrTKGT2wfu9vnt0j5SNin5O3xpSCuKhbGA
        
        if(sessionStorage.getItem('token')){
            sessionStorage.setItem('accessToken',sessionStorage.getItem('token'));
        }else if(sessionStorage.getItem('accessToken')){
            
        }
        else{
            this.router.navigate(['/sign-in']);
        }

    }
}
