import { Component, ViewEncapsulation } from '@angular/core';
import { Router, Navigation } from '@angular/router'
import { SweetAlertService } from '../../../../shared/sweetalert.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { EvaluateService } from 'app/services/evaluate.service';

@Component({
    selector: 'landing-evaluate-elderly',
    templateUrl: './elderly.component.html',
    encapsulation: ViewEncapsulation.None,

})

export class ElderlyComponent {
    userFullname: string;
    validateForm: boolean = false;
    rowsQuestions: any;
    question: any[] = [];
    myNgmodel = { items: [] };
    profilePatient: any;

    question_type_id:number = 19;
    is_old: boolean = false;

    constructor(
        private evaluateService: EvaluateService,
        private sweetAlertService: SweetAlertService,
        private spinner: NgxSpinnerService,
        private router: Router
    ) {
        this.userFullname = localStorage.getItem('userFullname');
        this.profilePatient = JSON.parse(localStorage.getItem('profileData'));
    }

    ngOnInit(): void {

        this.dataEvaluate();
    }

    async getEvaluate(id: any, question_type_id: any) {

        let info: any = {
            "coc_home_id": id,
            "question_type_id": question_type_id
        }

        try {
            let rs: any = await this.evaluateService.getEvaluate(info);
            // console.log(rs);
            return rs;
        } catch (error) {
            console.log(error);
        }

    }

    async dataEvaluate() {
        let id: any = {
            question_type_id: this.question_type_id
        };
    
        try {
            let rs: any = await this.evaluateService.list(id);
            // console.log(rs);
            this.rowsQuestions = rs.question;
            // console.log(this.rowsQuestions);

            let result = await this.getEvaluate(this.profilePatient.coc_home_id, this.question_type_id);
            if (result.length > 0) {
                this.is_old = true;
                result.forEach((data: any, index: any) => {
                    var jsonRow: object = {};
                    jsonRow['evauate_id'] = data['evauate_id'];
                    jsonRow['coc_home_id'] = data['coc_home_id'];
                    jsonRow['question_type_id'] = data['question_type_id'];
                    jsonRow['question_id'] = data['question_id'];
                    jsonRow['answer_id'] = data['answer_id'];
                    jsonRow['answer_text'] = data['answer_text'];
                    jsonRow['note_text'] = data['note_text'];
                    jsonRow['answer_score'] = data['answer_score'];
                    jsonRow['record_by'] = data['record_by'];
                    jsonRow['ngModel'] = '';
                    
                    this.question.push(jsonRow);
                });
                // console.log(this.question);
                this.rowsQuestions.forEach((data: any, index: any) => {
                    let index2 = this.question.findIndex(x => x.question_id === data['question_id']);
                    this.rowsQuestions[index].answer_id = this.question[index2].answer_id;
                    this.rowsQuestions[index].answer_text = this.question[index2].answer_text;
                    this.rowsQuestions[index].answer_score = this.question[index2].answer_score;
                    this.rowsQuestions[index].record_by = this.question[index2].record_by;
                    this.rowsQuestions[index].note_text = this.question[index2].note_text;
                });
                // console.log(this.rowsQuestions);
            } else {
                let dataQ = rs.question;
                // console.log(this.rowsQuestions);

                dataQ.forEach((data: any, index: any) => {
                    // key['index'] = val + 1;
                    // console.log(data);
                    // var jsonRow: object = data;
                    var jsonRow: object = {};
                    // delete jsonRow['answer'];  
                    jsonRow['coc_home_id'] = this.profilePatient.coc_home_id;
                    jsonRow['question_type_id'] = data['question_type_id'];
                    jsonRow['question_id'] = data['question_id'];
                    jsonRow['answer_id'] = '';
                    jsonRow['answer_text'] = '';
                    jsonRow['note_text'] = '';
                    jsonRow['answer_score'] = '';
                    jsonRow['record_by'] = '';
                    jsonRow['ngModel'] = '';

                    this.question.push(jsonRow);
                });
                // console.log(this.question);
            }
        } catch (error: any) {
            console.log(error.error);
        }

    }

    onRadioChange(e) {
        var ev = e;
        // console.log(ev);
        // console.log(this.question);
        // console.log(this.question2);

        var answer_id: number = Number(ev.source.id);
        var _question_id: number = Number(ev.source.name);
        // console.log(typeof (_question_id));
        // console.log(_question_id);

        let index = this.question.findIndex(x => x.question_id === _question_id);
        // console.log(index);
        this.question[index].answer_id = answer_id;
        this.question[index].answer_score = ev.value;
        this.question[index].answer_text = ev.source._elementRef.nativeElement.firstChild.innerText;
        this.question[index].record_by = this.userFullname;

        // console.log(this.question);

    }

    async saveEvaluate() {
        this.spinner.show();
        let info = this.question;
        // console.log(info);
        let validate = this.validateQuestion();
        // console.log(validate);
        if (validate) {
            let saveResult: boolean;
            for (let inf of info) {
                delete inf['ngModel'];
                try {
                    if(this.is_old){
                        this.question.forEach(async (data: any, index: any) => {
                            // console.log(data);
                            let rs: any = await this.evaluateService.updateEvaluate(data.evauate_id,data);
                        });                    
                    }else{
                        let rs: any = await this.evaluateService.saveEvaluate(inf);
                    // console.log(rs);
                    }
                    saveResult = true;
                } catch (error: any) {
                    saveResult = false;
                    console.log(error.error);
                    this.sweetAlertService.error('คำชี้แจง', 'ไม่สามารถบันทึกข้อมูลได้', 'SmartRefer Ubon');
                }
            }
            if (saveResult) {
                // this.sweetAlertService.success('คำชี้แจง', 'บันทึกข้อมูลเรียบร้อย', 'SmartRefer Ubon');
                this.sweetAlertService.toastSucces('คำชี้แจง', 'บันทึกข้อมูลเรียบร้อย', 'SmartRefer Ubon');
                this.router.navigate(['/evaluate']);
            } else {
                this.sweetAlertService.error('คำชี้แจง', 'ไม่สามารถบันทึกข้อมูลได้', 'SmartRefer Ubon');
            }

        } else {
            this.sweetAlertService.error('คำชี้แจง', 'กรุณากรอกข้อมูลให้ครบ', 'SmartRefer Ubon');

        }
        this.spinner.hide();
    }

    validateQuestion() {
        let validate: boolean = true;

        for (let data of this.question) {
            if (data.answer_id == '') {
                validate = false;
                break;
            }

        }
        return validate;
    }
}
