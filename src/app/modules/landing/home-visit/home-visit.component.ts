import { Component, ViewChild, ViewEncapsulation, Inject } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Dialog2q } from './dialog-2q/dialog-2q.component';
import { Dialog9q } from './dialog-9q/dialog-9q.component';
import { Dialog8q } from './dialog-8q/dialog-8q.component';
import { EvaluateData } from './evaluate';
import { HomevisitService } from '../../../services/homevisit.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { round } from 'lodash';
import { SweetAlertService } from '../../../shared/sweetalert.service';
import { RegisterService } from '../../../services/register.service';

import moment from "moment";
import * as _ from "lodash";


export interface FuseConfirmationConfig {
    title?: string;
    message?: string;
    icon?: {
        show?: boolean;
        name?: string;
        color?:
        | 'primary'
        | 'accent'
        | 'warn'
        | 'basic'
        | 'info'
        | 'success'
        | 'warning'
        | 'error';
    };
    actions?: {
        confirm?: {
            show?: boolean;
            label?: string;
            color?:
            | 'primary'
            | 'accent'
            | 'warn';
        };
        cancel?: {
            show?: boolean;
            label?: string;
        };
    };
    dismissible?: boolean;
}

@Component({
    selector: 'landing-home',
    templateUrl: './home-visit.component.html',
    //styleUrls: ['./home-visit.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class HomeVisitComponent {
    // ปฏิทิน
    selectedDateStart: any = { sdate: new Date() };


    sdate: any;
    edate: any;

    patient: any;
    result: any;

    evaluate: EvaluateData[] = [];

    // summary: string;

    coc_register_id: any;
    home_visit_date: any;
    //home_visit_date: any;
    present_gender: any;


    rsClicksave: any='';
    age: any;
    patient_address_name: any;
    phone_number: any;
    diag_text: any;
    evaluate_clause: any;
    extra_detail: any;
    patient_right_code: any;
    disablility_card: any;
    address_lat: any;
    address_long: any;
    care_giver_name: any;
    care_giver_contact: any;
    coc_status: any;
    gait_aid: any;
    home_item: any = [];
    med_use: any;
    med_use_problem: any;
    self_use: any;
    self_use_list: any;
    drugs_use: any;
    drugs_use_other: any;
    summary: any;
    problem_and_comment: any;
    result_code: any;
    result_name: any;
    plan_code: any;
    plan_name: any;
    body_temp: any;
    pulse_rate: any;
    respiratory_rate: any;
    sbp: any;
    dbp: any;
    o2sat: any;
    body_height: any;
    body_weight: any;
    bmi: any;
    dtx: any;
    genogram_id: any;
    uploadImageBase64: any;
    itemCatagory: any = [];
    itemItems: any = [];

    error_message: any = [];
    catagoryName: any;
    itemName: any;

    item = [
        { id: 1, name: 'Home Oxygen' },
        { id: 2, name: 'NG Tube' },
        { id: 3, name: 'Foley Catheter' },
        { id: 4, name: 'PCA' },
        { id: 5, name: 'IV Fluid' },
        { id: 6, name: 'Colostomy Bag' },
        { id: 7, name: 'CAPD' },
        { id: 8, name: 'Tracheostomy Tube' },
    ]

    question: any;
    question9q: any;
    question8q: any;
    result2q: any = [];
    result9q: any = [];
    result8q: any = [];

    comment_text: string = '';

    result2q_score: any = '';
    http: any;

    /**
     * Constructor
     */
    constructor(
        private router: Router,
        public dialog: MatDialog,
        private spinner: NgxSpinnerService,
        private sweetAlertService: SweetAlertService,
        private homevisitService: HomevisitService,
        private registerService: RegisterService,
    ) {
        let username = sessionStorage.getItem('username');
        if(!username){
            this.router.navigate(['/sign-in']);
        }
    }


    openDialog2q(): void {

        const dialogRef = this.dialog.open(Dialog2q, {
            width: '800px',
            data: { coc_register_id: this.patient.coc_register_id, question: this.question },
        },);

        dialogRef.afterClosed().subscribe(result => {
            let sum_score = 0;
            // console.log('The dialog was closed');
            this.result2q = result;
            this.result2q.forEach(element => {
                sum_score += parseInt(element.answer_score);
            });
            // console.log(sum_score);
            if (sum_score > 0) {
                this.result2q_score = '2Q : POSITIVE'
                this.openDialog9q();
            } else {
                this.result2q_score = '2Q : NEGATIVE'
            }
            // console.log(this.result2q);
        });
    }

    openDialog9q(): void {
        const dialogRef = this.dialog.open(Dialog9q, {
            width: '800px',
            data: { coc_register_id: this.patient.coc_register_id, question: this.question9q }
        },);

        dialogRef.afterClosed().subscribe(result => {
            let sum_score = 0;
            // console.log('The dialog was closed');
            this.result9q = result;
            this.result9q.forEach(element => {
                sum_score += parseInt(element.answer_score);
            });
            // console.log(sum_score);
            if (sum_score > 7) {
                this.openDialog8q();
                // console.log('pass question 9q');
            }
            this.result2q_score += ', 9Q : ' + sum_score;
            // console.log(this.result9q);
        });
    }

    openDialog8q(): void {
        const dialogRef = this.dialog.open(Dialog8q, {
            width: '800px',
            data: { coc_register_id: this.patient.coc_register_id, question: this.question8q }
        },);

        dialogRef.afterClosed().subscribe(result => {
            let sum_score = 0;
            // console.log('The dialog was closed');
            this.result8q = result;
            this.result8q.forEach(element => {
                sum_score += parseInt(element.answer_score);
            });
            this.result2q_score += ', 8Q : ' + sum_score;
            if (sum_score >= 17) {
                this.comment_text = 'ส่งต่อโรงพยาบาลมีจิตแพทย์ด่วน';
            }
            // console.log(sum_score);
        });
    }

    ngOnInit() {
        this.list_catagory();
        this.list_items();
        this.load_data();
        this.load_question2q();
        this.load_question9q();
        this.load_question8q();
    }

    async back() {
        let route = sessionStorage.getItem('route');
        this.router.navigate([route]);
    }

    async load_data() {
        try {
            this.patient = JSON.parse(sessionStorage.getItem('itemRegister'));
            this.coc_register_id = this.patient.coc_register_id;
            this.home_visit_date = this.patient.home_visit_date;
            // console.log(this.patient);
            
        } catch (error) {
            console.log(error.error);
        }
    }

    async list_catagory() {
        let rs: any = await this.registerService.list_catagory();
        this.itemCatagory = rs;
        const catagory_list = this.patient.evaluate_cause;
        if(catagory_list == null || catagory_list == '' || catagory_list == undefined) {
          this.catagoryName = '';
          return;
        }
        let catagory = catagory_list.split(',');
        let catagory_name: string = '';
        if(catagory.length >0) {
          catagory.forEach(element => {
            let rs: any = this.itemCatagory.find(x => x.catagory_id == element);
            if(rs.catagory_name != undefined){
              catagory_name += rs.catagory_name + ',';
            } 
          });
          this.catagoryName = catagory_name.substring(0, catagory_name.length - 1);
        }
      }
    
      async list_items() {
        let rs: any = await this.registerService.list_items();
        this.itemItems = rs;
        const item_list = this.patient.items;
        if(item_list == null || item_list == '' || item_list == undefined) {
          this.itemName = '';
          return;
        }
        let item = item_list.split(',');
        let item_name = '';
        if(item.length >0) {
          item.forEach(element => {
            let rs : any = this.itemItems.find(x => x.id == element);
            if(rs.name != undefined) {
              item_name += rs.name + ',';
            }
          });
          this.itemName = item_name.substring(0, item_name.length - 1);
        }
      }

    async save_coc_homevisit() {

        if (this.sdate == null) {
            this.home_visit_date = moment(new Date()).format("YYYY-MM-DD");
        } else {
            this.home_visit_date = this.sdate;
        }

        this.spinner.show();
        let info: any = {
            "coc_register_id": this.patient.coc_register_id,
            "home_visit_date": moment(this.home_visit_date).tz('Asia/Bangkok').format('YYYY-MM-DD'), 
            "present_gender": this.present_gender,
            "age": this.patient.age,
            "patient_address_name": this.patient.patient_address_name,
            "phone_number": this.patient.phone_number,
            "diag_text": this.patient.diag_text,
            "evaluate_clause": this.patient.evaluate_clause,
            "extra_detail": this.extra_detail,
            "patient_right_code": this.patient.patient_right_code,
            "disablility_card": this.disablility_card,
            "care_giver_name": this.care_giver_name,
            "care_giver_contact": this.care_giver_contact,
            "coc_status": this.coc_status,
            "gait_aid": this.gait_aid,
            "home_item": this.home_item.toString(),
            "med_use": this.med_use,
            "med_use_problem": this.med_use_problem,
            "self_use": this.self_use,
            "self_use_list": this.self_use_list,
            "drugs_use": this.drugs_use,
            "drugs_use_other": this.drugs_use_other,
            "result_name": this.result2q_score + ' ' + this.comment_text,
            "problem_and_comment": this.patient.problem_and_comment,
            "body_temp": parseFloat(this.body_temp),
            "pulse_rate": parseInt(this.pulse_rate),
            "respiratory_rate": parseInt(this.respiratory_rate),
            "sbp": parseInt(this.sbp),
            "dbp": parseInt(this.dbp),
            "o2sat": parseInt(this.o2sat),
            "body_height": parseInt(this.body_height),
            "body_weight": parseFloat(this.body_weight),
            "bmi": this.bmi,
            "dtx": parseInt(this.dtx)
        }



        // console.log('info',info);

        if (this.validate(info)) {
            try {
                if(this.rsClicksave == ''){
                    let rs: any = await this.homevisitService.save(info);
                    this.rsClicksave = rs;

                    //แก้ไข ที่อยู่ เบอร์โทร เพศสภาพ
                    let info_patient: any = {
                        "patient_address_name": this.patient.patient_address_name,
                        "phone_number": this.patient.phone_number,
                      }
                    //   console.log(info_patient);
                      
                      let rs_patient = await this.registerService.update(info_patient, this.patient.coc_register_id);
                    //  console.log(rs_patient);
                     
                }

                //ประเมิน 2Q ก่อน
                if (this.rsClicksave && this.result2q_score != '') {
                    if (this.result2q.length > 0) {
                        try {
                            this.result2q.forEach(async element => {
                                let evaluate: any = {
                                    "coc_home_id": this.rsClicksave,
                                    "question_id": element.question_id,
                                    "question_type_id": element.question_type_id,
                                    "answer_id": element.answer_id,
                                    "answer_score": element.answer_score,
                                    "answer_text": element.answer_text,
                                    "record_by": element.record_by,
                                }
                                let rs2q = await this.homevisitService.saveEvaluate(evaluate);
                                // console.log('res 2q:', rs2q);
                            });
                        } catch (error) {
                            console.log(error);
                        }
                    }
                    if (this.result9q.length > 0) {
                        try {
                            this.result9q.forEach(async element => {
                                let evaluate: any = {
                                    "coc_home_id": this.rsClicksave,
                                    "question_id": element.question_id,
                                    "question_type_id": element.question_type_id,
                                    "answer_id": element.answer_id,
                                    "answer_score": element.answer_score,
                                    "answer_text": element.answer_text,
                                    "record_by": element.record_by,
                                }
                                let rs9q = await this.homevisitService.saveEvaluate(evaluate);
                            });
                        } catch (error) {
                            console.log(error);
                        }
                    }
                    if (this.result8q.length > 0) {
                        try {
                            this.result8q.forEach(async element => {
                                let evaluate: any = {
                                    "coc_home_id": this.rsClicksave,
                                    "question_id": element.question_id,
                                    "question_type_id": element.question_type_id,
                                    "answer_id": element.answer_id,
                                    "answer_score": element.answer_score,
                                    "answer_text": element.answer_text,
                                    "record_by": element.record_by,
                                }
                                let rs8q = await this.homevisitService.saveEvaluate(evaluate);
                            });
                        } catch (error) {
                            console.log(error);
                        }
                    }
                    //กรณีประเมิน 2q 9q 8q
                    let i:any=localStorage.getItem('itemStorage')
                    this.router.navigate(['/sendhomevisitview']);
                    this.sweetAlertService.success('คำชี้แจง', 'บันทึกลงเยี่ยมบ้านสำเร็จ', 'SmartRefer Ubon');

                } else {
                    this.sweetAlertService.info('คำชี้แจง', 'กรุณาประเมิน 2Q', 'SmartRefer Ubon');
                }

            } catch (error) {
                console.log(error.error);
            }


        } else {
            this.sweetAlertService.error('คำชี้แจง', this.error_message.join(', '), 'SmartRefer Ubon');
        }
        //กรณีประเมิน 2Q อย่างเดียวให้มาทำงาน เพราะ 9q+8q  catch error
        if (this.rsClicksave !='' && this.result2q_score != ''){
            let i:any=localStorage.getItem('itemStorage')
            this.sweetAlertService.success('คำชี้แจง', 'บันทึกลงเยี่ยมบ้านสำเร็จ', 'SmartRefer Ubon');
            this.router.navigate(['/sendhomevisitview']);
        }

    }

    home_visit(data: any, e: any) {
        if (e.checked) {
            this.home_item.push(data);
        } else {
            this.home_item.splice(this.home_item.indexOf(data), 1);
        }
    }

    validate(data: any) {
        let validation: boolean = true;
        this.error_message = [];

        if (data.coc_status == null) {
            this.error_message.push('กรุณาเลือกสถานะ')
            validation = false;
        }

        if (this.coc_status == '1') {
            if (this.body_temp && isNaN(parseFloat(this.body_temp))) {
                this.error_message.push('อุณหภูมิร่างกาย ต้องเป็นตัวเลขเท่านั้น')
                validation = false;
            }
            if (this.sbp && isNaN(parseFloat(this.sbp))) {
                this.error_message.push('ความดันโลหิต ต้องเป็นตัวเลขเท่านั้น')
                validation = false;
            }
            if (this.dbp && isNaN(parseFloat(this.dbp))) {
                this.error_message.push('ความดันโลหิต ต้องเป็นตัวเลขเท่านั้น')
                validation = false;
            }
            if (this.pulse_rate && isNaN(parseFloat(this.pulse_rate))) {
                this.error_message.push('ชีพจร ต้องเป็นตัวเลขเท่านั้น')
                validation = false;
            }
            if (this.respiratory_rate && isNaN(parseFloat(this.respiratory_rate))) {
                this.error_message.push('การหายใจ ต้องเป็นตัวเลขเท่านั้น')
                validation = false;
            }
            if (this.o2sat && isNaN(parseFloat(this.o2sat))) {
                this.error_message.push('ออกซิเจนในเลือด ต้องเป็นตัวเลขเท่านั้น')
                validation = false;
            }
            if (this.body_height && isNaN(parseFloat(this.body_height))) {
                this.error_message.push('ส่วนสูง ต้องเป็นตัวเลขเท่านั้น')
                validation = false;
            }
            if (this.body_weight && isNaN(parseFloat(this.body_weight))) {
                this.error_message.push('น้ำหนัก ต้องเป็นตัวเลขเท่านั้น')
                validation = false;
            }
            if (this.dtx && isNaN(parseFloat(this.dtx))) {
                this.error_message.push('DTX ต้องเป็นตัวเลขเท่านั้น')
                validation = false;
            }
        }

        if (data.home_visit_date == null) {
            this.error_message.push('กรุณากรอกวันที่เยี่ยมบ้าน')
            validation = false;
        }
        if (data.care_giver_name == null) {
            this.error_message.push('กรุณากรอกชื่อผู้ดูแล')
            validation = false;
        }
        if (data.care_giver_contact == null) {
            this.error_message.push('กรุณากรอกเบอร์โทรผู้ดูแล')
            validation = false;
        }
        if (data.med_use == null) {
            this.error_message.push('กรุณาเลือกการใช้ยา')
            validation = false;
        }
        if (data.self_use_list == null) {
            this.error_message.push('กรุณาเลือกการใช้ยาเอง')
            validation = false;
        }
        if (data.drugs_use == null) {
            this.error_message.push('กรุณาเลือกการใช้สารเสพติด')
            validation = false;
        }

        return validation;
    }

    bmiCal() {
        if (this.body_height != null && this.body_weight != null) {
            this.bmi = round((this.body_weight / (this.body_height * this.body_height)) * 10000, 1);
        }
    }

    async load_question2q() {
        let rs: any;
        try {
            rs = await this.homevisitService.question2q();
            this.question = rs.question;
            //            console.log(this.question);
        } catch (error) {
            console.log(error);
        }
    }

    async load_question9q() {
        let rs: any;
        try {
            rs = await this.homevisitService.question9q();
            this.question9q = rs.question;
            //console.log(this.question9q);
        } catch (error) {
            console.log(error);
        }
    }

    async load_question8q() {
        let rs: any;
        try {
            rs = await this.homevisitService.question8q();
            this.question8q = rs.question;
            //console.log(this.question9q);
        } catch (error) {
            console.log(error);
        }
    }

    async onFileSelected(event) {
        let file = event.target.files[0];
        let reader = new FileReader();
        await reader.readAsDataURL(file);
        reader.onload = () => {
            // console.log(reader.result);
            // this.uploadImageBase64 = reader.result;
            const uploadImageBase64: string | ArrayBuffer = reader.result;
            this.uploadImageBase64 = {
                coc_register_id: this.coc_register_id,
                genogram: uploadImageBase64
            };
            // console.log(this.uploadImageBase64);
        };

        reader.onerror = function (error) {
            console.log('Error: ', error);
        };

        if (!this.genogram_id) {
            try {
                const rs: any = await this.homevisitService.uploadFile(this.uploadImageBase64);
                // console.log(rs);
                this.genogram_id = rs[0];
                if (rs.ok == false) {
                    this.sweetAlertService.error('คำชี้แจง', 'ไม่สามารถบันทึกข้อมูลได้', 'SmartRefer Ubon');
                } else {
                    this.sweetAlertService.success('คำชี้แจง', 'บันทึกข้อมูลเรียบร้อยแล้ว', 'SmartRefer Ubon');
                }
            } catch (error) {
                console.log(error);
            }
        } else {
            try {
                const rs: any = await this.homevisitService.updateGenogram(this.genogram_id, this.uploadImageBase64);
                if (rs.ok == false) {
                    this.sweetAlertService.error('คำชี้แจง', 'ไม่สามารถบันทึกข้อมูลได้', 'SmartRefer Ubon');
                } else {
                    this.sweetAlertService.success('คำชี้แจง', 'บันทึกข้อมูลเรียบร้อยแล้ว', 'SmartRefer Ubon');
                }
            } catch (error) {
                console.log(error);
            }

        }



    }

    dateChangeStart(e: any) {
        // console.log("date change start");
        if (e.startDate != null) {
            let syear = e.startDate.$y;
            let smonth = String(e.startDate.$M + 1).padStart(2, "0");
            let sday = String(e.startDate.$D).padStart(2, "0");
            this.sdate = [syear, smonth, sday].join("-");
            //   console.log(this.sdate);
        }
    }

    catagoryDescription(catagory_list:string){
        if(catagory_list == null || catagory_list == '' || catagory_list == undefined) {
          return '';
        }
        let catagory = catagory_list.split(',');
        let catagory_name: string = '';
        if(catagory.length >0) {
          catagory.forEach(element => {
            let rs: {catagory_id: number, catagory_name: string} = this.itemCatagory.find(x => x.catagory_id == element);
            if(rs.catagory_name != undefined){
              catagory_name += rs.catagory_name + ',';
            } 
          });
        }
        return catagory_name.substring(0, catagory_name.length - 1);
      }
    
      itemDescription(item_list:string){
        if(item_list == null || item_list == '' || item_list == undefined) {
          return '';
        }
        let item = item_list.split(',');
        let item_name = '';
        if(item.length >0) {
          item.forEach(element => {
            let rs : { id:number, name:string }= this.itemItems.find(x => x.id == Number(element));
            if(rs.name != undefined) {
              item_name += rs.name + ',';
            }
          });
        }
    
        return item_name.substring(0, item_name.length - 1);
      }
}
