import { Route } from '@angular/router';
import { CocComponent } from 'app/modules/landing/coc/coc.component';

export const cocRoutes: Route[] = [
    {
        path     : '',
        component: CocComponent
    }
];
