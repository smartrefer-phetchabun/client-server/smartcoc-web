import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { HomeVisitViewComponent } from 'app/modules/landing/home-visit-view/home-visit-view.component';
import { homeVisitViewRoutes } from 'app/modules/landing/home-visit-view/home-visit-view.routing';
import {FormGroup, FormControl} from '@angular/forms';
import {Component} from '@angular/core';

@NgModule({
    declarations: [
        HomeVisitViewComponent
    ],
    imports     : [
        RouterModule.forChild(homeVisitViewRoutes),
        SharedModule
    ]
})
export class HomeVisitViewModule
{
    
}
