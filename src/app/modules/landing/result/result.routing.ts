import { Route } from '@angular/router';
import { ResultComponent } from 'app/modules/landing/result/result.component';

export const resultRoutes: Route[] = [
    {
        path     : '',
        component: ResultComponent
    }
];
