export interface Register {
    coc_register_id: number;
    refer_hospcode: string;
    refer_to_hospcode: string;
    title: string;
    first_name: string;
    middle_name: string;
    last_name: string;
    cid: string;
    dob: string;
    age: string;
    patient_address_name: string;
    tel: string;
    patient_right: string;
    discharge_date: string;
    diag_text: string;
}