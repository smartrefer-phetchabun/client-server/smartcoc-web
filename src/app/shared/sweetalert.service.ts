import { Injectable } from '@angular/core';
import { default as swal, SweetAlertOptions } from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class SweetAlertService {

  constructor() { }

  error(title: any, text: any = 'เกิดข้อผิดพลาด', foot: any) {

    const option: SweetAlertOptions = {
      icon: 'error',
      title: title,
      text: text,
      footer: foot,
      confirmButtonText: 'ตกลง'
    };
    swal.fire(option);

  }


  info(title: any, text: any = 'เกิดข้อผิดพลาด', foot: any) {

    const option: SweetAlertOptions = {
      icon: 'info',
      title: title,
      text: text,
      footer: foot,
      confirmButtonText: 'ตกลง'
    };
    swal.fire(option);

  }

  success(title: any, text: any = 'คำชื้แจง', foot: any) {

    const option: SweetAlertOptions = {
      icon: 'success',
      title: title,
      text: text,
      footer: foot,
      timer: 2000,
      confirmButtonText: 'ตกลง'
    };
    swal.fire(option)
      .then(
        function () { },
        // handling the promise rejection
        function (dismiss) {
          if (dismiss === 'timer') { }
        }
      )
  }
  dialogSuccess(title: any, text: any = 'คำชี้แจ้ง', foot: any) {
    const option: SweetAlertOptions = {
      position: 'center',
      icon: 'success',
      title: title,
      text: text,
      footer: foot,
      timer: 1500,
      showConfirmButton: false,
    };
    swal.fire(option)
      .then(
        function () { },
        // handling the promise rejection
        function (dismiss) {
          if (dismiss === 'timer') { }
        }
      )

  }
  dialogError(title: any, text: any = 'เกิดข้อผิดพลาด', foot: any) {
    const option: SweetAlertOptions = {
      position: 'center',
      icon: 'error',
      title: title,
      text: text,
      footer: foot,
      timer: 1500,
      showConfirmButton: false,
    };
    swal.fire(option)
      .then(
        function () { },
        // handling the promise rejection
        function (dismiss) {
          if (dismiss === 'timer') { }
        }
      )
  }

  async confirm(title: any, text = 'คุณต้องการดำเนินการนี้ ใช่หรือไม่?', foot: any) {
    console.log('fon');
    
    const option: SweetAlertOptions = {
      // icon: 'warning',
      title: title,
      text: text,
      footer: foot,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'ใช่, ดำเนินการ!',
      cancelButtonText: 'ยกเลิก',
    };

    let result = await swal.fire(option);
        if (result.dismiss) return false;
        if (result.value) return true;

        return false;
  }
  toastError(title: any, text: any = 'เกิดข้อผิดพลาด', foot: any) {
    const option: SweetAlertOptions = {
      icon: 'error',
      title: title,
      text: text,
      footer: foot,
      timer: 2000,
      toast: true,
      position: 'bottom-right',
      showConfirmButton: false,
    };
    swal.fire(option)
      .then(
        function () { },
        // handling the promise rejection
        function (dismiss) {
          if (dismiss === 'timer') { }
        }
      )
  }
  toastSucces(title: any, text: any = 'เกิดข้อผิดพลาด', foot: any) {
    const option: SweetAlertOptions = {
      icon: 'success',
      title: title,
      text: text,
      footer: foot,
      timer: 2000,
      toast: true,
      position: 'bottom-right',
      showConfirmButton: false,
    };
    swal.fire(option)
      .then(
        function () { },
        // handling the promise rejection
        function (dismiss) {
          if (dismiss === 'timer') { }
        }
      )
  }

}