import { Component, Inject, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
    message: string;
    error_code?: string;
}

@Component({
    selector: 'alert',
    templateUrl: './alert.component.html',
    encapsulation: ViewEncapsulation.None
})

export class DialogAlert {
    
    dialogalertForm : UntypedFormGroup;

    constructor(
        public matDialogRef: MatDialogRef<DialogAlert>,
        private _formBuilder: UntypedFormBuilder,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
    ) { }

    /**
     * Save and close
     */
     saveAndClose(): void
     {
         // Save the message as a draft
         this.saveAsDraft();
 
         // Close the dialog
         this.matDialogRef.close();
     }
 
     /**
      * Discard the message
      */
     discard(): void
     {
 
     }
 
     /**
      * Save the message as a draft
      */
     saveAsDraft(): void
     {
 
     }
 
     /**
      * Send the message
      */
     send(): void
     {
 
     }
}